from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)


POSTGRES = {
    "user": "postgres",
    "pw": "1234",
    "db": "flask",
    "host": "localhost",
    "port": "5432",
}


app.config["SQLALCHEMY_DATABASE_URI"] = (
    "postgresql://%(user)s:\
%(pw)s@%(host)s:%(port)s/%(db)s"
    % POSTGRES
)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SECRET_KEY"] = "Sm9obiBTY2hyb20ga2lja3MgYXNz"


db = SQLAlchemy(app)


@app.route("/")
def hello():
    return "Hello!"
